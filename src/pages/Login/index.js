/* eslint-disable react/display-name */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, {
  useState,
  useEffect,
  useRef,
  useCallback,
  currentRoute,
} from 'react';
import useLocalStorage from '../../components/Utils/LocalStorage';
import useAuth from '../../hooks/useAuth';
import useCompareRef from '../../hooks/useCompareRef';
import './index.scss';

const Login = ({
  setCurrentRoute,
  pageSelection,
  setLoginStatus,
  loginStatus,
  setUserData,
}) => {
  let userLogin = {
    identifier: '',
    password: '',
  };

  const inputRef = useRef(null);

  useEffect(() => {
    const token = localStorage.getItem('token');
    const user = JSON.parse(localStorage.getItem('user'));
    if (token) {
      setLoginStatus('SessionON');
      setUserData(user);
    }
  }, []);

  useEffect(() => {
    inputRef.current.focus();
  }, []);

  const handleChange = (page) => {
    setCurrentRoute(page);
  };

  const loginFunction = useAuth(setLoginStatus);

  const handleSubmit = async (e) => {
    e.preventDefault();
    const userData = await loginFunction.login(userLogin);

    if (userData) {
      setUserData(userData);
      setLoginStatus('SessionON');
      useLocalStorage('token', userData.jwt);
      useLocalStorage('user', JSON.stringify(userData));

      setTimeout(() => {
        setCurrentRoute('Home');
      }, 1500);
    } else {
      setLoginStatus('Error');
    }

  };


  const contraryState = useCompareRef(loginStatus);

  return (
    <div className={`login-${pageSelection}`}>
      <div className='containerLogin'>
        <form
          className={`formLogin ${contraryState === 'SessionON' ? 'ON' : ''}`}
          onSubmit={handleSubmit}
        >
          <h1>LOGIN</h1>

          <div className='form-group'>
            <label>User:</label>
            <input
              type='text'
              className='form-control'
              placeholder='First Name'
              onChange={(e) => (userLogin.identifier = e.target.value)}
              ref={inputRef}
            ></input>
          </div>

          <div className='form-group'>
            <label>Password</label>
            <input
              type='password'
              className='form-control'
              placeholder='Password'
              onChange={(e) => (userLogin.password = e.target.value)}
            ></input>
          </div>

          <div className='buttons'>
            <button className='btn'> Login</button>
            <a onClick={() => handleChange('SingUp')} className='link'>
              If you are not registered, click here
            </a>
          </div>
        </form>
        <div
          className={`${loginStatus === 'Error' ? 'errorActivate' : 'errorOff'
            } `}
        >
          <h2 className='errMessage'> User or password incorrect!</h2>
        </div>
      </div>
    </div>
  );
};
export default Login;
