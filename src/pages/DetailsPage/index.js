/* eslint-disable react/display-name */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, memo, useCallback, useEffect } from 'react';
import Comment from '../../components/Comment';
import AddComment from '../../components/Comment/AddComment';
import './index.scss';

const DetailsPage = ({ pageSelection, userData, loginStatus }) => {
  const [newComments, setSNewComments] = useState(false);
  const [addComment, setAddComment] = useState('OFF');
  let dataGame = localStorage.getItem('game');
  let gameParse = JSON.parse(dataGame);
  const numberOfComments = gameParse?.comments.length;

  const handleClick = () => {
    setAddComment('ON');
  };

  return (
    <div className={`details-${pageSelection}`}>
      <h1 className='title-details'>{gameParse?.title}</h1>
      <div className='details-products'>
        <div className='detail-product'>
          <a href='#h'>
            <div className='details-img'>
              <img src={gameParse?.imgUrl} alt={gameParse?.id}></img>
            </div>
          </a>
          <div className='details-footer'>
            <h1> {gameParse?.title} </h1>
            <p className='detail-body'> {gameParse?.body} </p>
            <p className='detail-platform'>DATE: {gameParse?.date}</p>
            <p className='detail-platform'>CATEGORY: {gameParse?.category}</p>
            <p className='detail-platform'>PRICE: ${gameParse?.price}</p>
          </div>
          <div className='detail-btn'>
            <a className='detail-btn' href={gameParse?.gameUrl}>
              <button className='btn-det'>OFFICIAL SITE</button>
            </a>
          </div>

          <div
            className={`${addComment === 'OFF' ? 'comment-visible' : 'comment-invisible'
              }`}
          >
            <Comment
              id={gameParse?.id}
              numberOfComments={numberOfComments}
              newComments={newComments}
            ></Comment>
          </div>

          <div
            className={`${addComment === 'ON' ? 'commentBox' : 'comment-invisible'
              }`}
          >
            <AddComment
              userData={userData}
              loginStatus={loginStatus}
              setAddComment={setAddComment}
              gameParse={gameParse}
              setSNewComments={setSNewComments}
              newComments={newComments}
            ></AddComment>
          </div>

          <div
            className={`${addComment === 'OFF' ? 'button' : 'comment-invisible'
              }`}
          >
            <button className='btn-comment' onClick={handleClick}>
              Comment
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default DetailsPage;
