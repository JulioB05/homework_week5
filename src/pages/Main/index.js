/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { memo } from 'react';
import './index.scss';
import DataFetchNext from '../Products';

const Main = memo(
  ({ currentPage, setCurrentRoute, pageSelection, setApiState, apiState }) => {
    return (
      <div className={`main-${pageSelection}`}>
        {currentPage ? (
          <DataFetchNext
            currentPage={currentPage}
            setCurrentRoute={setCurrentRoute}
            setApiState={setApiState}
            apiState={apiState}
          />
        ) : (''
        )}
      </div>
    );
  }
);

export default Main;
