/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { useState, memo } from 'react';
import useLocalStorage from '../Utils/LocalStorage.js';
import './index.scss';

const Card = memo((props) => {
  const {
    id,
    title,
    imgUrl,
    gameUrl,
    body,
    price,
    date,
    category,
    comments,
    setCurrentRoute,
    apiState,
  } = props;
  const [clicked, setClicked] = useState(false);

  const handleClick = () => {
    setClicked(!clicked);
  };

  const localStorageData = () => {
    let infoGame = {
      id: id,
      title: title,
      imgUrl: imgUrl,
      gameUrl: gameUrl,
      body: body,
      price: price,
      date: date,
      category: category,
      comments: comments,
    };
    useLocalStorage('game', JSON.stringify(infoGame));
    useLocalStorage('stateDetails', true);
  };

  const functionClick = (e) => {
    e.preventDefault();
    localStorageData();
    setCurrentRoute('Details');
  };

  return (
    <div className='product'>
      <a href={gameUrl}>
        <div className='product_img'>
          <img
            src={
              apiState === 'Arrived' ? imgUrl : 'https://i.gifer.com/3sqI.gif'
            }
            alt=''
          ></img>
        </div>
      </a>
      <div className='product_footer'>
        <h1> {title} </h1>
        <p className='body'> {body} </p>
        <p className='platform'>{date}</p>
        <p className='platform'>{category}</p>
      </div>
      <div className='button'>
        <button
          className='btn'
          clicked={clicked.toString()}
          onClick={handleClick}
        >
          ${price}
        </button>

        <div>
          <a href='' className='btn' onClick={functionClick}>
            Details
          </a>
        </div>
      </div>
    </div>
  );
});

export default Card;
