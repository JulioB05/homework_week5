/* eslint-disable react/prop-types */
import React from 'react';
import Pagination from '../Pagination';
import './index.scss';

const Footer = ({
  currentPage,
  nextPage,
  prevPage,
  setCurrentPage,
  pageSelection,
}) => {
  return (
    <div className={`footer-${pageSelection}`}>
      <Pagination
        currentPage={currentPage}
        nextPage={nextPage}
        prevPage={prevPage}
        setCurrentPage={setCurrentPage}
        pageSelection={pageSelection}
      ></Pagination>
    </div>
  );
};

export default Footer;
