/* eslint-disable react/prop-types */
import React from 'react';
import './index.scss';

const Loader = ({ apiState }) => {
  return (
    <div className={apiState === 'loading' ? 'loading' : 'Arrived'}>
      LOADING...
      <span className='loaderStyle'> </span>
    </div>
  );
};

export default Loader;
