/* eslint-disable react/display-name */
/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useMemo, memo, useCallback } from 'react';
import useDataLength from '../../hooks/useDataLength';
import './index.scss';

const Pagination = memo(({ currentPage, setCurrentPage, pageSelection }) => {
  const sizeData = useDataLength(
    'https://trainee-gamerbox.herokuapp.com/games?_sort=id'
  );

  let maxNumberPage = useMemo(() => {
    return Math.ceil(sizeData / 6);
  }, [sizeData]);

  const arrayPages = [];
  for (let i = 0; i < maxNumberPage; i++) {
    let a = {
      id: i + 1,
      numberPage: i + 1,
    };

    arrayPages.push(a);
  }

  const [pagePosition, setPagePosition] = useState(currentPage);
  const [clicked, setClicked] = useState('false');

  const HandleChange = (page) => {
    setCurrentPage(page);
    setPagePosition(page);
  };

  const nextPage = () => {
    const nextPage = currentPage + 1;

    if (currentPage < maxNumberPage) {
      setCurrentPage(nextPage);
      setPagePosition(nextPage);
      setClicked(true);
    }
  };

  const prevPage = () => {
    let prevPage = currentPage - 1;
    if (currentPage > 1) {
      setCurrentPage(prevPage);
      setPagePosition(prevPage);
      setClicked(true);
    }
  };

  return (
    <div className={pageSelection}>
      <div className='pagination'>
        <button className='BtnNextPrev' onClick={prevPage}>
          PREVIOUS
        </button>

        {arrayPages.length
          ? arrayPages?.map((page) => {
            const { id, numberPage } = page;
            return (
              <button
                key={id}
                clicked={clicked.toString()}
                onClick={() => HandleChange(numberPage)}
                className={`pageNumberList ${pagePosition === id && clicked ? ' active' : ''
                  }`}
              >
                {numberPage}
              </button>
            );
          })
          : null}

        <button className='BtnNextPrev' onClick={nextPage}>
          NEXT
        </button>
      </div>
    </div>
  );
});

export default Pagination;
