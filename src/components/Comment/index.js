/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useState, useEffect, useCallback } from 'react';
import './index.scss';

const Comment = ({ id, numberOfComments, newComments }) => {
  let URL = `https://trainee-gamerbox.herokuapp.com/games/${id}/comments?_limit=${numberOfComments}`;
  const [comments, setComments] = useState([]);
  const [apiCommentsState, setApiCommentsState] = useState('loading');

  useEffect(() => {
    let controller = new AbortController();
    (async () => {
      try {
        setApiCommentsState('loading');

        const response = await fetch(`${URL}`);

        if (response.status === 200) {
          const data = await response.json();
          let orderData = data
            .slice()
            .sort((a, b) => new Date(a.created_at) - new Date(b.created_at));
          setComments(orderData);
          setApiCommentsState('Arrived');
          controller = null;
        } else if (response.status === 400) {
          console.error('Bad Request');
        }
      } catch (e) {
        console.error(e);
      }
    })();

    return () => controller?.abort();
  }, [URL, newComments]);


  // const getComments = useCallback(() => {
  //   (async () => {
  //     try {
  //       setApiCommentsState('loading');

  //       const response = await fetch(`${URL}`);

  //       if (response.status === 200) {
  //         const data = await response.json();
  //         let orderData = data
  //           .slice()
  //           .sort((a, b) => new Date(a.created_at) - new Date(b.created_at));
  //         setComments(orderData);
  //         setApiCommentsState('Arrived');

  //       } else if (response.status === 400) {
  //         console.error('Bad Request');
  //       }
  //     } catch (e) {
  //       console.error(e);
  //     }
  //   })();
  // }, [URL, newComments]);



  return (
    <div className='commentContainer'>
      <div className={`${apiCommentsState === 'Arrived' ? '' : 'invisible'}`}>
        {comments.length
          ? comments?.map((comment) => {
            let dateParse = Date.parse(comment?.updated_at);
            let data = new Date(dateParse);

            return (
              <div key={comment?.id}>
                <div className='container-comments'>
                  <div className='comments'>
                    <div className='info-comments'>
                      <div className='header'>
                        <h4>
                          <span className='title'>Username:</span>
                          {comment?.user?.username}
                        </h4>
                        <h5>{data.toDateString()}</h5>
                      </div>

                      <p>{comment?.body}</p>
                    </div>
                  </div>
                </div>
              </div>
            );
          })
          : null}
      </div>
      <span
        className={`${apiCommentsState === 'loading' ? 'loader' : 'invisible'}`}
      ></span>
    </div>
  );
};

export default Comment;
