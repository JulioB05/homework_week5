/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useEffect, useState, useRef, useCallback } from 'react';
import '../index.scss';

const AddComment = ({
  userData,
  loginStatus,
  setAddComment,
  gameParse,
  setSNewComments,
  newComments,
}) => {
  const date = new Date();
  const currentDate =
    String(date.getDate()).padStart(2, '0') +
    '/' +
    String(date.getMonth() + 1).padStart(2, '0') +
    '/' +
    date.getFullYear();

  const inputRef = useRef(null);
  useEffect(() => {
    inputRef.current.focus();
  });

  const userInformationLogged = {
    token: userData?.jwt,
    body: '',
    idGame: gameParse?.id,
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setAddComment('OFF');
    setSNewComments(!newComments);

    try {
      const response = await fetch(
        `https://trainee-gamerbox.herokuapp.com/games/${userInformationLogged?.idGame}/comment`,
        {
          headers: {
            Accept: ' */*',
            'Content-Type': 'application/json',
            Authorization: `Bearer ${userInformationLogged.token}`,
          },
          method: 'POST',

          body: JSON.stringify({ body: userInformationLogged.body }),
        }
      );

      if (response.status === 200) {
        const data = await response.json();
      } else if (response.status === 400) {
        console.error('Bad Request');
      }
    } catch (e) {
      console.error(e);
    }
  };

  return (
    <div className='commentContainer'>
      <div key='{comment.id}'>
        <form className='container-comments' onSubmit={handleSubmit}>
          <div className='comments'>
            <div className='info-comments'>
              <div className='header'>
                <h4>
                  <span className='title'>Username:</span>
                  {loginStatus === 'SessionON'
                    ? userData?.user?.username
                    : 'UNREGISTERED USER'}
                </h4>
                <h5>{currentDate}</h5>
              </div>

              <div className='form-group'>
                <input
                  ref={inputRef}
                  type='text'
                  className='form-comment'
                  placeholder='Write your comment here'
                  onChange={(e) =>
                    (userInformationLogged.body = e.target.value)
                  }
                ></input>
              </div>

              <footer className='footer-comment'>
                <div className='footer-btn'>
                  <button
                    className={`${loginStatus === 'SessionON'
                      ? 'btn-send-comment'
                      : 'invisible'
                      }`}
                  >

                    Send Comment
                  </button>
                  <p
                    className={`${loginStatus === 'SessionON' ? 'invisible' : ''
                      }`}
                  >

                    ERROR: You have to login to comment
                  </p>
                </div>
              </footer>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddComment;
