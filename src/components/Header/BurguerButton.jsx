/* eslint-disable react/display-name */
/* eslint-disable react/prop-types */
import React, { memo } from 'react';
import '../../index.scss';

const BurguerButton = memo(({ handleClick, clicked }) => {
  return (
    <div
      onClick={handleClick}
      className={`icon nav-icon-5 ${clicked ? 'open' : ''}`}
    >
      <span></span>
      <span></span>
      <span></span>
    </div>
  );
});
export default BurguerButton;
