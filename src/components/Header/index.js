/* eslint-disable react/prop-types */
import React, { useEffect, useState } from 'react';
import './index.scss';
import BurguerButton from './BurguerButton';

const Header = ({ setCurrentRoute, loginStatus, userData, setLoginStatus }) => {
  const [clicked, setClicked] = useState(false);

  useEffect(() => { }, []);

  const handleClick = () => {
    setClicked(!clicked);
  };

  const handleChange = (page) => {
    setCurrentRoute(page);
  };

  const logoutUser = () => {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    setLoginStatus('SessionOFF');
  };

  return (
    <header>
      <div className='navContainer'>
        <h2>
          VideoGames <span> Stores </span>
        </h2>
        <div
          className={`${loginStatus === 'SessionON' ? 'loggedUser' : 'invisible'
            }`}
        >
          <p>
            User: {userData.user?.firstName +
              ' ' +
              userData.user?.lastName}
          </p>
        </div>
        <div className={`links ${clicked ? 'active' : ''}`}>
          <a onClick={() => handleChange('Home')}>Home</a>
          <a href='https://www.freetogame.com/'>Oficial Site</a>

          <a onClick={() => handleChange('Login')}>
            {`${loginStatus === 'SessionOFF' ? 'Login' : ''}`}
            {`${loginStatus === 'Error' ? 'Login' : ''}`}
          </a>
          <a onClick={() => handleChange('SingUp')}>
            {`${loginStatus === 'SessionOFF' ? 'SingUp' : ''}`}
            {`${loginStatus === 'Error' ? 'SingUp' : ''}`}
          </a>
          <a onClick={logoutUser}>
            {`${loginStatus === 'SessionON' ? 'Logout' : ''}`}
          </a>
        </div>

        <div className='burguer'>
          {/* Estas son las props */}
          <BurguerButton
            clicked={clicked}
            handleClick={handleClick}
          ></BurguerButton>
        </div>

        <div className={`BgDiv initial ${clicked ? ' active' : ''}`}></div>
      </div>
    </header>
  );
};

export default Header;
