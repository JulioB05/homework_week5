import { useRef, useEffect } from 'react';

const useCompareRef = (state) => {
  const classSelect = useRef('visible');

  useEffect(() => {
    switch (state) {
      case 'SessionOn':
        classSelect.current = 'invisible';
        break;

      case 'SessionOFF':
        classSelect.current = 'visible';
        break;

      case 'Error':
        classSelect.current = 'Error';
        break;

    }
  }, [state]);

  return classSelect.current;

};

export default useCompareRef;
