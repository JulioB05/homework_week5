/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';

const baseURL = 'https://trainee-gamerbox.herokuapp.com/auth/local';

const useAuth = (setLoginStatus) => {
  const [dataUser, setDataUser] = useState('');

  const login = async (userLogin) => {
    try {
      const response = await fetch(baseURL, {
        headers: {
          Accept: ' */*',
          'Content-Type': 'application/json',
        },
        method: 'POST',
        body: JSON.stringify(userLogin),
      });

      if (response.status === 200) {
        const data = await response.json();
        setDataUser(data);
        return data;
      } else if (response.status === 400) {
        console.error('Bad request');
        setLoginStatus('Error');
      }
    } catch (e) {
      console.error(e);
    }
  };
  return {
    login,
    dataUser,
  };
};

export default useAuth;
