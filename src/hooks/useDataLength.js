import { useState, useEffect } from 'react';

const useDataLength = (URL) => {
  const [dataSize, setDataSize] = useState([]);

  useEffect(() => {
    (async () => {
      const response = await fetch(`${URL}`);
      const data = await response.json();
      let number = data.length;
      setDataSize(number);
    })();

  }, [URL]);

  return dataSize;
};

export default useDataLength;